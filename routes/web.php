<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'IndexController@home');
Auth::routes();

Route::get('/kategori/create', 'KategoriController@create');
Route::get('/kategori', 'KategoriController@index');
Route::get('/kategori/{kategori_id}', 'KategoriController@show');
Route::post('/kategori', 'KategoriController@store');
Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
Route::put('/kategori/{kategori_id}', 'KategoriController@update');
Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');


// CRU - PROFILE
Route::get('/profile', 'ProfileController@index');

 // Create 
Route::get('/profile/create', 'ProfileController@create'); //Route menuju form create
Route::post('/profile', 'ProfileController@store'); // Route untuk menyimpan form ke database
Route::get('/profile/edit', 'ProfileController@edit'); // Route Untuk edit Data
// Read
Route::get('/profile', 'ProfileController@index'); // Route untuk read data
Route::get('/profile/{profile_id}', 'ProfileController@show'); //Menampilkan detail

//Update
Route::put('/profile/{profile_id}', 'ProfileController@update'); //Route untuk update data


Route::get('/tema', function () {
    return view('tema');
});

// CRUD Pertanyaan
// Create
Route::get('/pertanyaan', 'tanyaController@index');
Route::get('/pertanyaan/create', 'tanyaController@create');
Route::post('/pertanyaan', 'tanyaController@store');

Route::get('/pertanyaan/{pertanyaan_id}', 'tanyaController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'tanyaController@edit');
Route::put('/pertanyaan/{pertanyaan_id}', 'tanyaController@update');
Route::delete('/pertanyaan/{pertanyaan_id}', 'tanyaController@destroy');

// // // CRUD Jawaban
// // Route::get('/pertanyaan', 'JawabController@index');
// Route::get('/pertanyaan/{pertanyaan_id}', 'JawabController@create');
// Route::post('/pertanyaan/{pertanyaan_id}', 'JawabController@store');

// Route::get('/pertanyaan/{pertanyaan_id}', 'JawabController@show');
// // Route::get('/pertanyaan/{pertanyaan_id}/edit', 'JawabController@edit');
// // Route::put('/pertanyaan/{pertanyaan_id}', 'JawabController@update');
// // Route::delete('/pertanyaan/{pertanyaan_id}', 'JawabController@destroy');