<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tanya extends Model
{
    protected $table='pertanyaan';
    protected $fillable=['isi', 'gambar', 'user_id', 'kategori_id'];

    public function kategori() {
        return $this->belongsTo('App\Kategori', 'kategori_id');
    }
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function jawab() {
        return $this->hasMany('App\Jawab', 'pertanyaan_id');
    }
}
