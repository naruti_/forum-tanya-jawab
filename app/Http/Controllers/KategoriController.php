<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = Kategori::all();
        return view('kategori.index', compact('kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // dd('create');
        return view('kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nama' => 'required',
        ]);
        $kategori = new Kategori;
        $kategori->nama = $request->nama;
        $kategori->user_id = 1;
        // dd($kategori);

        $kategori->save();
        return redirect('/kategori');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($kategori_id)
    {
        //
        $kategori = Kategori::where('id', $kategori_id)->first();
        // $kategori = kategori::find($id);
        return view('kategori.show', compact('kategori'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kategori_id)
    {
        $kategori = Kategori::where('id', $kategori_id)->first();
        // $kategori = kategori::find($id);
        return view('kategori.edit', compact('kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kategori_id)
    {
        //
        // dd($request);
        $request->validate([
            'nama' => 'required',
        ]);
        // dd('update');

        $kategori = Kategori::find($kategori_id);
        $kategori->nama = $request->nama;
        // dd($kategori);
        // $kategori->umur = $request->umur;
        // $kategori->bio = $request->bio;
        $kategori->save();

        return redirect('/kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kategori_id)
    {
        $kategori = Kategori::find($kategori_id);
        $kategori->delete();

        return redirect('/kategori');
    }
}
