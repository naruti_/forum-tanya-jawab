<?php

namespace App\Http\Controllers;

use App\Kategori;
use App\Tanya;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TanyaController extends Controller
{
    public function index()
    {
        $pertanyaan = Tanya::all();
        // dd($pertanyaan);
        return view('pertanyaan.list-tanya', compact('pertanyaan'));
    }

    public function create()
    {   
        // $tanya->user_id = Auth::user()->id;
        // dd(Auth::user()->id);
        $kategori = Kategori::all();
        return view('pertanyaan.form-tanya', compact('kategori'));
    }
    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            'isi' => 'required',
            'kategori' => 'required',
        ]);
        $tanya = new Tanya;
 
        $tanya->isi = $request->isi;
        $tanya->gambar = $request->gambar;
        
        $tanya->user_id = Auth::user()->id;
        $tanya->kategori_id=$request->kategori;

        $tanya->save();

        return redirect('/pertanyaan');

        // dd($request->all());
    }
    public function show ($pertanyaan_id) {
        // dd('show');
        $tanya= Tanya::where('id', $pertanyaan_id)->first();

        return view('pertanyaan.detail-tanya', compact('tanya'));
    }
}
