<?php

namespace App\Http\Controllers;
use App\Profil;
use App\User;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    // Create GET
    public function create(){
        return view('profile.create');
    }

    // Create POST
    public function store(Request $request){
        // nama, umur, dan bio tidak boleh kosong.
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        // Untuk mengirim ke database(phpMyAdmin)
        $profile = new Profil;
        $profile->nama = $request->nama;
        $profile->umur = $request->umur;
        $profile->bio = $request->bio;
 
        $profile->save();

        return redirect('/');
    }

    // Read GET --- Menampilkan Tabel saja
    public function index(){

        $profile = Profil::all();
       
        return view('profile.index', compact('profile'));
    }

    // Read GET --- Menampilkan detail beserta ID.
    public function show($profile_id){

        $profile = Profil::where('id', $profile_id)->first();
        return view('profile.show', compact('profile'));
    }

    // Update GET --- Untuk edit data
    public function edit(){
        // dd('')
        $user_id = Auth::user()->id;
        // dd($user_id);

        $profile = Profil::where('users_id', $user_id)->first();
        // dd($profile);
        // dd($profile->user->email);
        return view('profile.edit', compact('profile'));
    }

    // Update PUT --- untuk edit data dan mengembalikan/input datanya kembali.
    public function update(Request $request, $profile_id){

        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $profile = Profil::find($profile_id);
        $profile->nama = $request['nama'];
        $profile->umur = $request['umur'];
        $profile->bio = $request['bio'];

        $userId = $profile->users_id;
        $user = User::find($userId);
        $user->name = $request['nama'];
        $user->save();
        // dd($userId);
 
        $profile->save();

        return redirect('/');
    }
}
