<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori'; 
    protected $fillable = ['nama', 'user_id'];

    public function tanya() {
        return $this->hasMany('App\Tanya');
    }
}
