<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawab extends Model
{
    //
    protected $table='jawab';
    protected $fillable=['isi', 'gambar', 'user_id', 'pertanyaan_id'];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function tanya() {
        return $this->belongsTo('App\Tanya', 'pertanyaan_id');
    }
}
