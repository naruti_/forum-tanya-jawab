<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table = 'profile';

    protected $fillable = ['nama', 'umur', 'bio', 'users_id'];

    public function user() {
        return $this->belongsTo('App\User', 'users_id');
    }
}
