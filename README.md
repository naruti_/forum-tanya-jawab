# Project 1

## Kelompok 16

Dibuat oleh kelompok 16 program PKS Digital School Batch 2

<br>

## Anggota Kelompok

Format penulisan => Nama Lengkap / username gitlab

-   Rhama Fahri Afriansyah / rhamafahri
-   Ahrozu Junda Mudafi'ul Islam / ajmudafiulislam
-   Dhia Aziz Rizqi Arrahman / naruti\_

<br>

## Tema Project

Forum tanya jawab dalam lingkup pendidikan

<br>

## ERD
<img src="erd.jpg">
<br>

## Link Terkait

Link Demo Aplikasi: https://drive.google.com/file/d/1N4s5rML1qqoHSOjGJwB40I1vUYrr9UyS/view?usp=sharing

Link Deploy: https://forum-tanya-jawab-16.herokuapp.com/
