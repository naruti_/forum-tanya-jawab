<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="/" class="brand-link">
            <img src="https://thumbs.dreamstime.com/b/green-talk-bubble-chat-forum-symbol-logo-design-vector-graphic-116976997.jpg"
                class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">#FORUM-tanya-jawab</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                @auth
                <div class="image">
                    <img
                        src="https://thumbs.dreamstime.com/b/head-question-answer-illustration-art-questions-logo-isolated-background-39625504.jpg">
                </div>
                <div class="info">
                    <a href="/profile/edit" class="d-block">{{ Auth::user()->name }}</a>
                </div>
                @endauth
                @guest
                <div class="info">
                    <a href="/login" class="d-block">Anda Belum Login</a>
                </div>
                @endguest
            </div>



            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="/" class="nav-link">
                            <i class="nav-icon fas fa-home"></i>
                            <p>
                                Home
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/kategori" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Kategori
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/pertanyaan" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Pertanyaan
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Tables
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="/data-table" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>DataTables</p>
                                </a>
                            </li>

                        </ul>

                    </li>
                    @auth
                    <li class="nav-item">
                        <a class="nav-link bg-danger" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                            <i class="nav-icon fas fa-arrow-alt-circle-right"></i>

                            {{-- <i class="nav-icon fas fa-battery-quarter"></i> --}}
                            {{-- <i class="nav-icon fas .fa-arrow-alt-circle-left"></i> --}}
                            {{-- <i class="fa-solid fa-alien"></i> --}}
                            {{-- <i class="fa-regular fa-arrow-right-from-bracket"></i> --}}
                            <p>
                                {{ __('Logout') }}
                            </p>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                    @endauth
                    @guest
                    <li class="nav-item">
                        <a href="/login" class="nav-link">
                            <i class="nav-icon fas fa-arrow-alt-circle-right"></i>

                            <p>
                                Login
                            </p>
                        </a>
                    </li>
                    @endguest
                </ul>

            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
</body>

</html>