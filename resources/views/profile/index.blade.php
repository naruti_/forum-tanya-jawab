@extends('layout.master')

@section('title')
Halaman Detail Profile
@endsection

@section('content')

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col">Action</th>
    </tr>
  </thead>

  <tbody>
    @forelse ($profile as $key => $item)
    <tr>
      <td>{{$key + 1}}</td>
      <td>{{$item->nama}}</td>
      <td>{{$item->umur}}</td>
      <td>{{$item->bio}}</td>
      <td>

        <form action="/profile/{{$item->id}}" method="POST">
          <a href="/profile/{{$item->id}}" class="btn btn-info">Detail</a>
          <a href="/profile/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
          @csrf
          @method('DELETE')
          <input type="submit" class="btn btn-danger btn-sm" value="Delete">
        </form>
      </td>
    </tr>
    @empty

    @endforelse
  </tbody>

</table>
@endsection