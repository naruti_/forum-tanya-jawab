@extends('layout.master')

@section('title')
YOUR PROFILE
@endsection

@section('content')
<form action="/profile" method="POST">
  @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Nama</label>
    <input type="text" name="nama" class="form-control">
  </div>
  @error('nama')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="exampleInputPassword1">Umur</label>
    <input type="number" name="umur" class="form-control">
  </div>
  @error('umur')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="exampleInputPassword1">Bio</label>
    <input type="text" name="bio" class="form-control">
  </div>
  @error('bio')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary"> <a href="/profile"></a>Submit</button>
</form>
@endsection