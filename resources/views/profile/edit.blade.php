@extends('layout.master')

@section('title')
Edit Profile
@endsection

@section('content')

<form action="/profile/{{$profile->id}}" method="POST">
  @csrf
  @method('PUT')
  <div class="form-group">
    <label for="exampleInputEmail1">Nama Profile</label>
    <input type="text" name="nama" value="{{$profile->nama}}" class="form-control">
  </div>
  @error('nama')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input type="email" name="email" value="{{$profile->user->email}}" class="form-control" disabled>
  </div>
  @error('nama')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="exampleInputPassword1">Umur</label>
    <input type="number" name="umur" value="{{$profile->umur}}" class="form-control">
  </div>
  @error('umur')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="exampleInputPassword1">Bio</label>
    <input type="text" name="bio" value="{{$profile->bio}}" class="form-control">
  </div>
  @error('bio')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary"> <a href="/profile"></a>EDIT</button>
</form>

@endsection