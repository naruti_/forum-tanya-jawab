@extends('layout.master')

@section('content')
    <form action="/pertanyaan" method="post">
        @csrf
        <div class="form-group mt-5 mx-5 px-5" style="border-radius-30px">
            <label for="exampleInputEmail1">
                <h3>Pertanyaan</h3>
            </label>
            <textarea class="form-control" name="isi" rows="10" placeholder="Masukkan Pertanyaanmu di sini"></textarea> <br>
            @error('isi')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <label>
                <p>Kategori</p>
            </label>
            <select class="custom-select" id="inputGroupSelect01" name="kategori">
                <option selected></option>
                @foreach ($kategori as $item)
                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                @endforeach
                {{-- <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option> --}}
            </select>
          
            @error('kategori')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            {{-- <input type="text" name="test" id=""> --}}

            <div>
                <!-- <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="katagori" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Katagori
                                </button>
                                <div class="dropdown-menu" style="colour-#fd7e14" aria-labelledby="dropdownMenu2">
                                    <button class="dropdown-item" type="button">Action</button>
                                    <button class="dropdown-item" type="button">Another action</button>
                                    <button class="dropdown-item" type="button">Something else here</button>
                                </div> -->
            </div><br>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection
