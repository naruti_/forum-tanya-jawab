@extends('layout.master')

@section('judul')
    {{-- Detail Pertanyaan --}}
@endsection

@section('content')
    
    <h1> {{ $tanya->isi }}</h1>
    <form action="/pertanyaan/{pertanyaan_id}" method="post">
        <input type="text" name="jawab" class="form-control"> <br>
        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="">
            Jawab
        </button>
    </form> <br>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Isi</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    <h2>Jawaban</h2>
    @foreach ($tanya->jawab as $item)
        <div class="card">
            <div class="card-body">
                <p>{{ $item->isi }}</p>
                <p class="text-secondary">Dijawab oleh: {{ $item->user->name }}</p>
            </div>
        </div>
    @endforeach
@endsection
