@extends('layout.master')

@section('content')
    <a href="pertanyaan/create" class="mb-3 ml-3 btn btn-secondary">Buat Pertanyaan Baru</a>

    <ul class="list-group list-group-flush">

        @forelse($pertanyaan as $key => $item)
            <li class="list-group-item">
                <span class="badge badge-success">{{ $item->kategori->nama }}</span>
                <p class="text-secondary"><small>Ditanyakan oleh: {{ $item->user->name}}</small></p>
                <p class="lead"><strong>{{ $item->isi }}</strong></p>

                <p>
                    {{-- <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
           Jawab Pertanyaan
        </a> --}}
                    <a class="btn btn-primary" href="/pertanyaan/{{ $item->id }}" role="button" aria-expanded="false"
                        aria-controls="collapseExample">
                        Jawab Pertanyaan
                    </a>

                </p>
                <div class="collapse" id="collapseExample">
                    <input type="text" name="jawab" class="form-control"> <br>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample"
                        aria-expanded="false" aria-controls="collapseExample">
                        Kirim
                    </button>
                </div>
            </li>
        @empty
            <li>Data tidak ada</li>
        @endforelse
        {{-- <li class="list-group-item">Text Pertanyaan <br>
        
        <p>
        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
           Jawab Pertanyaan
        </a>
        
        </p>
        <div class="collapse" id="collapseExample">
            <input type="text" name="jawab" class="form-control"> <br>
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Kirim
            </button>
        </div>
    </li> --}}

    </ul>
@endsection
