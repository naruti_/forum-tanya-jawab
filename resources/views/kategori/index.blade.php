@extends('layout.master')

@section('judul')
    Index Kategori
@endsection

@push('script')
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#tableData").DataTable();
        });
    </script>
@endpush
@push('link')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
    <style>
        /* .dataTables_filter {
            width: 50%;
            float: right;
            text-align: right; !important
        } */

    </style>
@endpush

@section('content')
    {{-- @auth --}}
    <a href="kategori/create" class="mb-3 btn btn-secondary">Tambah Kategori</a>
    {{-- @endauth --}}
    <table class="table table-bordered table-striped" id="tableData">
        <thead class="">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($kategori as $key => $item)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $item->nama }}</td>
                    <td>
                        {{-- @auth --}}
                            <form action="/kategori/{{ $item->id }}" method="POST">
                                <a href="/kategori/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                                <a href="/kategori/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                                @csrf
                                @method('DELETE')
                                <button onclick="return confirm('Apakah anda yakin akan menghapus kategori {{ $item->nama }}')" type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                        {{-- @endauth --}}
                        {{-- @guest
                            <a href="/kategori/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>

                        @endguest --}}
                    </td>

                </tr>
            @empty
                <h1>Data tidak ada</h1>
            @endforelse


        </tbody>
    </table>
@endsection
