@extends('layout.master')
@section('judul')
    Edit Kategori
@endsection
@section('content')
    <form action="/kategori/{{$kategori->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Nama Cast</label>
            <input type="text" name="nama" class="form-control" value="{{ $kategori->nama }}">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
       

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
