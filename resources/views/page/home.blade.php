@extends('layout.master')

@section('judul')
    SELAMAT DATA DI #FORUM-tanya-jawab
@endsection

@section('content')
    <div class="tampilanhome1" style="text-align: center">

        <img src="https://media.istockphoto.com/photos/questions-and-answers-qa-session-concept-picture-id532569720?k=20&m=532569720&s=612x612&w=0&h=ou9pCNJWznURLvJwdTXw_VkbMsRVbeJLvDwYZBs52xA="
            alt="">
        @auth
            <h1>Selamat Datang <span class="font-weight-bold">{{ Auth::user()->name }}</span></h1>
            <h3>Ayo mulai kontribusi di #FORUM-tanya-jawab </h3>
            <a href="/" class="btn btn btn-primary">Mulai kontribusi</a>
        @endauth
        @guest

            <h1>Selamat Datang </h1>
            <h3>Silahkan Login atau Register pada link dibawah ini!</h3>

            <div class="link" style="justify-content: center;">
                <a href="/login">
                    <h1>LOGIN</h1>
                </a>
                <p>
                <H1> - </H1>
                </p>
                <a href="/register">
                    <h1>REGISTER</h1>
                </a>
            </div>
        @endguest
    </div>


    </form>
@endsection
