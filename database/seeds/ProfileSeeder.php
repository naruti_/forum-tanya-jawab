<?php

use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                'nama' => 'Si Paling Tahu Segalanya',
                'users_id' => 1,
                'umur' => 1,
                'bio' => 'kosong',

                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'nama' => 'Orang ke 2',
                'users_id' => 2,
                'umur' => 100,
                'bio' => 'kosong',

                'created_at' => now(),
                'updated_at' => now(),
            ],
            
            
        ])->each(function ($profile) {
            DB::table('profile')->insert($profile);
            // User::create($user);
        });
    }
}
