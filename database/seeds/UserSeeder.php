<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;



class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                'name' => 'Si Paling Tahu Segalanya',
                'email' => 'test@gmail.com',
                'password' => Hash::make('password'),
                'email_verified_at' => Carbon::now(),
                'created_at' => now(),
                'updated_at' => now(),

            ],
            [
                'name' => 'Orang ke 2',
                'email' => 'test2@gmail.com',
                'password' => bcrypt('password'),
                'email_verified_at' => Carbon::now(),
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ])->each(function ($user) {
            DB::table('users')->insert($user);
            // User::create($user);
        });

        // User::factory(3)->create();
    }
}
