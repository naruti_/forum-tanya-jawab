<?php

use Illuminate\Database\Seeder;

class JawabSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                'isi' => 'Wah ini susah, tapi sepertinya jawabannya diatas 0',
                'user_id' => 1,
                'pertanyaan_id' => 1,
                'gambar' => 'kosong',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'isi' => 'Yang jelas jawabannya bukan 0, saya tidak tahu cara menghitungnya, saya masih TK',
                'user_id' => 2,
                'pertanyaan_id' => 1,
                'gambar' => 'kosong',

                'created_at' => now(),
                'updated_at' => now(),
            ],
            
            
        ])->each(function ($jawab) {
            DB::table('jawab')->insert($jawab);
            // User::create($user);
        });
    }
}
