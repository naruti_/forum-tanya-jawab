<?php

use Illuminate\Database\Seeder;

class PertanyaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                'isi' => 'Tolong bantu hitungkan dan jelaskan hasil dari 5+5+5+5+6+5+5-200x5?',
                'user_id' => 1,
                'kategori_id' => 1,
                'gambar' => 'kosong',

                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'isi' => 'Apa sinonim dari sinonim?',
                'user_id' => 2,
                'kategori_id' => 2,
                'gambar' => 'kosong',

                'created_at' => now(),
                'updated_at' => now(),
            ],
            
            
        ])->each(function ($pertanyaan) {
            DB::table('pertanyaan')->insert($pertanyaan);
            // User::create($user);
        });
    }
}
